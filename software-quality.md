footer: @5minpause — holgerfrohloff.de


# Software Quality

---

> The value of a developer shouldn't
> be tied to amount or quality of code.

## You are not your code.

---

# What is (software) quality?

![right, fit](quality-merriam-webster.png)

---

# [fit] The extent to which certain
# [fit] attributes/characteristics are pronounced

![](raquel-martinez-96648-unsplash.jpg)

---

![](charles-732126-unsplash.jpg)

* decide on quality attributes
* measure
* compare and rank to evaluate

---

# Why does it matter?

Simply put: 
Software or code of higher quality is _better_. 

Better in what regard?
In part that depends on the metrics you care about.

---

# Simple code:

1. Passes all the tests.
2. Expresses every idea that we need to express.
3. Says everything OnceAndOnlyOnce.
4. Has no superfluous parts. 

-- Kent Beck

^
- one of the creators of the eXtreme Programming methodology
- one of 17 signatories of the original Agile Manifesto
- also "invented" Test Driven Development.

---

![inline](Sandi Metz on Twitter.png)

---

![fit, left](ross-findon-303091-unsplash.jpg)

## Simple code makes software:

* easier to understand
* simpler to change
* more fun to work with
* saves money and time in the long run
* reduces risk of software projects

---

# How can you measure it?

---

![](agence-olloweb-520914-unsplash.jpg)

# [fit] How can you measure it?

- Linting
- Static Analysis
- Behavioral Analysis

---

## Linters

- Editor/IDE or CLI

![inline](linter.png)

---

## Static Analysis

- CLI or CI

![fit](/Users/holger/Desktop/Screenshot 2019-04-03 at 13.31.15.png)  
![fit](/Users/holger/Desktop/Screenshot 2019-04-03 at 13.31.32.png)

---

## Behavioral Analysis

- Externally with reports

![fit](/Users/holger/Desktop/Screenshot 2019-04-03 at 13.41.35.png)
![fit](/Users/holger/Desktop/Screenshot 2019-04-03 at 13.43.38.png)

--- 

# Tools

![inline, fit](Screenshot 2019-04-03 at 14.05.02.png)![inline, fit](Screenshot 2019-04-03 at 14.06.23.png)![](Screenshot 2019-04-03 at 14.05.25.png)
![](Screenshot 2019-04-03 at 14.06.10.png)![](Screenshot 2019-04-03 at 14.05.11.png)![](Screenshot 2019-04-03 at 14.06.42.png)
![](Screenshot 2019-04-03 at 14.07.54.png)![](Screenshot 2019-04-03 at 14.08.36.png)![](Screenshot 2019-04-03 at 14.11.19.png)
![](7aa5a1d-Screen_Shot_2017-07-26_at_4.55.43_PM.png)![](d0UJigrvTdKsNNsCLgXH_brakeman.png)![](FkT8PRePQZKRuTCJEXWd_2015-11-23_1856.png)

(Ask me for a list with links)