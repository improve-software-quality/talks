# Software Quality

---

## Every application is a future legacy application.

^
You start with good intentions on a green field project.
It never turns out the way you envisioned it.
External factors, internal factors.

^ talk more about risks of bad software

![](sqi/jeremy-cai-1169-unsplash.jpg)

---


## Writing good code is hard

- It takes time
- It takes experience
- It requires domain knowledge
- The better your communication the better your team’s result!

![right, fit](Sandi Metz on Twitter.png)

---


## You are not your code!

> The value of a developer shouldn’t be tied
> to the amount or the quality of code.

---

## Sometimes you only notice it some years later

If I look back at old code, I usually learn a lot about myself.

Ward Cunningham’s original definition of Technical debt:

> [...] a cycle where our understanding grows so that one day in the future we see a better way and put it in.

![right](sqi/austin-chan-275638-unsplash.jpg)

^ Many different ideas

---

## Many different ideas try to help and make it easier

* Sandi Metz
* Uncle Bob author of the book
“Clean Code” & SOLID rules
* Kent Beck
* Ron Jeffries
* Ward Cunningham

…and many, many more


^ I am by far not the first to speak on this subject. There are many books written and talks given. There is the "Clean Code" and "Code Craftsmanship" idea. A big proponent of this is

^ Single Responsibility
Open-Closed (extension, modification)
Liskov substitution (objects replaced by subtypes, still correct)
Interface segregation (small, focused interfaces)
dependency inversion (depend on abstractions)

---

## Simple Code — The 4 rules by Kent Beck

* Pass all tests
* Clear, expressive, & consistent
* Duplicates no behavior or configuration
* Minimal methods, classes, & modules

^ Duplication of knowledge, not code!
 Other rules perhaps even more known to Ruby programmers are the Rules by Sandi Metz

![left, fit](sqi/TheFourCommandments.png)

---

## The rules by Sandi Metz


![fit, inline](sqi/Screenshot 2019-04-03 at 16.51.20.png)

^ Why rules?

---

![fit, left](sqi/ross-findon-303091-unsplash.jpg)

## Simple code makes software:

* easier to understand
* simpler to change
* more fun to work with
* saves money and time in the long run
* reduces risk of software projects

^ You could run with either of these lists of rules, or you

---

## Create your own rules

![](sqi/mark-duffel-422279-unsplash.jpg)

<br />
<br />
<br />
<br />
<br />
<br />
<br />
Bound to happen with time.

^ You adapt existing rules and see what works best for you and your team. This will be heavily influenced by whoever you happen to work with 😉

^ If you have your own rules, you could use tools to help you achieve your goals and stick to the rules.


---

![](sqi/agence-olloweb-520914-unsplash.jpg)

## Use tools to help you measure
<br />

* Linters
* Static Analysis
* Behavioral Analysis


---

## Linters

- Editor/IDE or CLI

![inline, fit](sqi/linter.png)

---

## Static Analysis

- CLI or CI

![inline](/Users/holger/Desktop/Screenshot 2019-04-03 at 13.31.32.png)![inline, fit](/Users/holger/Desktop/Screenshot 2019-04-03 at 13.31.15.png)

---

## Behavioral Analysis

- Externally with reports

![inline](/Users/holger/Desktop/Screenshot 2019-04-03 at 13.41.35.png)![fit](/Users/holger/Desktop/Screenshot 2019-04-03 at 13.43.38.png)

---

## Every application is a ~~future legacy~~ application.

Stick to the rules, evolve, YAGNI your way to a better future.
